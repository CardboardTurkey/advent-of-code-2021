use std::{
    fs::File,
    io::{prelude::*, BufReader},
    path::Path,
};

fn lines_from_file(filename: impl AsRef<Path>) -> Vec<i32> {
    let file = File::open(filename).expect("no such file");
    let buf = BufReader::new(file);
    // let mut my_vec: Vec<i32> = Vec::new();
    // for line in buf.lines() {
    //     let line = line.expect("you fucked up");
    //     my_vec.push(line.parse().expect("Not int"));
    // }
    // return my_vec;
    buf.lines()
        .map(|l| l.expect("Could not parse line").parse().expect("Line not int"))
        .collect()
}

// ---

fn main() {
    let depths = lines_from_file("./src/input.txt");
    
    // First problem
    let mut last_depth = 2147483647;
    let mut incs = 0;
    for depth in depths.clone() {
        if depth > last_depth {
            incs += 1;
        }
        last_depth = depth;
    }
    println!("Number of increases: {}", incs);

    // Second problem
    let mut slices: Vec<i32> = Vec::new();
    for i in 0..depths.len() {
        if i+3>depths.len() {
            break;
        }
        slices.push(depths[i..i+3].iter().sum());
    }
    let mut last_slice = 2147483647;
    let mut slice_incs = 0;
    for slice in slices {
        if slice > last_slice {
            slice_incs += 1;
        }
        last_slice = slice;
    }
    println!("Number of increases: {}", slice_incs);

}